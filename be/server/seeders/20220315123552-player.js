"use strict";
const { hashPassword } = require("../utils/passwordHandler");

module.exports = {
  async up(queryInterface, _) {
    await queryInterface.bulkInsert("Players", [
      {
        username: "Davit",
        email: "davit@gmail.com",
        password: await hashPassword("davitaridani"),
        experience: 800000,
        lvl: 300,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        username: "Dani",
        email: "ari@gmail.com",
        password: await hashPassword("dani"),
        experience: 300000,
        lvl: 245,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  async down(queryInterface, _) {
    await queryInterface.bulkDelete("Players", null, {});
  },
};
