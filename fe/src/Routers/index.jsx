import React from 'react'
import { Routes, Route } from "react-router-dom";
import Create from "../components/Create";
import Update from "../components/Update";
import ListTable from "../components/ListTable";

const Routers = () => {
	return (
		<Routes>
			<Route path='/' element={<ListTable />} />
			<Route path='/create' element={<Create />} />
			<Route path="/update/:id" element={<Update />} />
		</Routes>
	)
}

export default Routers
