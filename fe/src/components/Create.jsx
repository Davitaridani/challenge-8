import React, { useState } from 'react'
import { Container, Row } from "react-bootstrap";
import Form from 'react-bootstrap/Form';
import axios from "axios";
import { apiUser } from "../utils/api";
import { Link, useNavigate } from "react-router-dom";

const Create = () => {
	const [data, setData] = useState({
		username: "",
		email: "",
		lvl: "",
		experience: ""
	})
	const navigate = useNavigate()

	const handleChange = (e) => {
		const valueData = e.target.value;
		setData(valueData)
	}

	const handleSubmit = async (e) => {
		e.preventDefault();

		const dataUser = {
			username: data.username,
			email: data.email,
			lvl: data.lvl,
			experience: data.experience,
		}
		try {
			const resData = await axios.post(`${apiUser}`, dataUser)

			console.log(resData);
			setData(resData)
			navigate("/")
		} catch (err) {
			console.log(err);
		}
	}

	return (
		<>
			<Container className='pt-5'>
				<h3 className='text-center mb-3'>Add User</h3>
				<Row className="justify-content-center">

					<div className="col-7">
						<Form onSubmit={handleSubmit}>
							<Form.Group className="mb-3" >
								<Form.Control type="text" placeholder="Username" name="username" onChange={handleChange} value={data.username || ""} />
							</Form.Group>
							<Form.Group className="mb-3" >
								<Form.Control type="email" placeholder="Email" name="email" onChange={handleChange} value={data.email || ""} />
							</Form.Group>
							<Form.Group className="mb-3" >
								<Form.Control type="number" placeholder="Level" name="lvl" onChange={handleChange} value={data.lvl || ""} />
							</Form.Group>
							<Form.Group className="mb-3" >
								<Form.Control type="text" placeholder="Experience" name="experience" onChange={handleChange} value={data.experience || ""} />
							</Form.Group>

							<div className="d-flex align-items-center gap-2">
								<button type='submit' className='btn btn-sm btn-primary'>Submit</button>
								<Link to="/" className="btn btn-sm btn-success">
									Back
								</Link>
							</div>
						</Form>
					</div>

				</Row>
			</Container>
		</>
	)
}

export default Create
