import React from 'react'
import { useParams } from "react-router-dom";

const Update = () => {

	const { id } = useParams();
	return (
		<>
			<Container className='pt-5'>
				<h3 className='text-center mb-3'>Add User</h3>
				<Row className="justify-content-center">
					<div className="col-7">
						<Form >
							<Form.Group className="mb-3" >
								<Form.Control type="text" placeholder="Username" name="username" />
							</Form.Group>
							<Form.Group className="mb-3" >
								<Form.Control type="email" placeholder="Email" name="email" />
							</Form.Group>
							<Form.Group className="mb-3" >
								<Form.Control type="number" placeholder="Level" name="lvl" />
							</Form.Group>
							<Form.Group className="mb-3" >
								<Form.Control type="text" placeholder="Experience" name="experience" />
							</Form.Group>

							<div className="d-flex align-items-center gap-2">
								<button type='submit' className='btn btn-sm btn-primary'>Submit</button>
								<Link to="/" className="btn btn-sm btn-success">
									Back
								</Link>
							</div>
						</Form>
					</div>

				</Row>
			</Container>
		</>
	)
}

export default Update
