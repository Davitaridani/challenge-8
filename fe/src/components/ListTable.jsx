import React, { useState, useEffect } from 'react'
import axios from "axios";
import { Link } from "react-router-dom";
import { Container } from "react-bootstrap";
import { apiUser } from "../utils/api";
import Table from 'react-bootstrap/Table';
import { BsPersonFillAdd } from "react-icons/bs";
import { HiOutlinePencilAlt } from "react-icons/hi";

import Search from './UI/Search';

const ListTable = () => {
	const [data, setData] = useState([]);
	const [isLoading, setIsLoading] = useState(true);
	const [isError, setIsError] = useState(false);
	const [search, setSearch] = useState(data)

	const getAllData = async () => {
		try {
			const resData = await axios.get(`${apiUser}`)
			setData(resData.data.data);
			setSearch(resData.data.data);
		} catch (err) {
			setIsError(err)
		} finally {
			setIsLoading(false);
		}
	}

	const searchUsername = (e) => {
		const searchValue = e.target.value;
		setSearch(
			data.filter((item) =>
				item.username.toLowerCase().includes(searchValue.toLowerCase())
			)
		);
	};

	const searchEmail = (e) => {
		const searchValue = e.target.value;
		setSearch(
			data.filter((item) =>
				item.email.toLowerCase().includes(searchValue.toLowerCase())
			)
		);
	};

	useEffect(() => {
		getAllData()
	}, [])

	if (isLoading) return <h3 className='text-center pt-5'>Loading...</h3>
	else if (data && !isError)
		return (
			<Container className='pt-5'>
				<div className="items-search d-flex align-items-center gap-3">
					<Link to="/create" className='btn btn-sm btn-success'>
						<span className='d-flex align-items-center gap-1'>
							<BsPersonFillAdd />
							Add
						</span>
					</Link>
					<Search resulSearch={searchUsername} placeholder="Search Username..." />
					<Search resulSearch={searchEmail} placeholder="Search Email..." />

				</div>
				<Table striped bordered hover className='mt-3'>
					<thead >
						<tr className='bg-secondary'>
							<th width="50px">NO</th>
							<th>Username</th>
							<th>Email</th>
							<th>Level</th>
							<th>Exp</th>
							<th className='text-center'>
								action
							</th>
						</tr>
					</thead>
					<tbody>
						{search?.map((item, i) => (
							<tr key={item.id}>
								<td>{i + 1}</td>
								<td>{item.username}</td>
								<td>{item.email}</td>
								<td>{item.lvl}</td>
								<td>{item.experience}</td>
								<td className='text-center'>
									<button className='btn btn-success btn-sm text-center'>
										<span className='text-center d-flex align-items-center'>

											<HiOutlinePencilAlt />
										</span>
									</button>
								</td>
							</tr>
						))}
					</tbody>
				</Table>
			</Container >
		)
	else {
		return <h3 className='text-center text-danger pt-5'>Something Went Wrong</h3>;
	}
}

export default ListTable
