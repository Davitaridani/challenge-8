import React from 'react'
import { BiSearchAlt2 } from "react-icons/bi";

const Search = ({ resulSearch, placeholder }) => {

	return (
		<div className="input-group w-25">
			<input
				type="text"
				className="form-control"
				placeholder={placeholder}
				onChange={resulSearch}
			/>
			<div className="input-group-text bg-secondary text-white">
				<BiSearchAlt2 />
			</div>
		</div>
	)
}

export default Search
